FROM ruby:2.6.3-alpine3.9

ENV LANG C.UTF-8

RUN apk add --no-cache zsh tini
WORKDIR /hanami-app


COPY Gemfile Gemfile.lock /hanami-app/
RUN bundle install

COPY . /hanami-app

ENTRYPOINT ["/bin/zsh", "./script/docker-entrypoint.sh"]

CMD ["hanami"]
